package excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.BuiltinFormats;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFDataFormat;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * 
 * @author Anthony Couture
 * 
 * <p>
 * 	Cette classe permet de creer un fichier Excel, lui appliquer un style, une couleur, des bordures, des valeurs dans des celulles et une formule <br>
 * 	Realiser pour des celulles avec des entiers et d'autre avec la device euro
 * </p>
 *
 */
public class JavaExcel {
	
	/**
	 * @author Anthony Couture
	 * Variable ou est stocké le style generale
	 */
	private static XSSFCellStyle styleGenerale;
	/**
	 * @author Anthony Couture
	 * Variable ou est stocké le document
	 */
	private static XSSFWorkbook workbook;
	/**
	 * @author Anthony Couture
	 * Variable permettant d'avoir le signe euro
	 */
	private static XSSFDataFormat dataformat;
	/**
	 * @author Anthony Couture
	 * Variable pour définir le nombre de ligne à initialiser
	 */
	private static int nbLigne;
	/**
	 * @author Anthony Couture
	 * Variable pour définir le nombre de colonne à initialiser
	 */
	private static int nbColonne;
	/**
	 * @author Anthony Couture
	 * Variable pour stocker toutes les variables initialiser
	 */
	private static List<Row> liste;
	
	/**
	 * @author Anthony Couture
	 * 
	 * @param workbook Variable du document excel
	 * @return un style qu'on peut appliquer a une cellule
	 * 
	 * <p>Cette fonction permet d'avoir un style pour mettre les cellules en blanc<p>
	 */
	public static XSSFCellStyle createStyleGeneral(XSSFWorkbook workbook) {
		XSSFFont font = workbook.createFont();
		font.setBold(true);
		XSSFCellStyle style = workbook.createCellStyle();
		style.setFont(font);
		style.setFillForegroundColor(IndexedColors.WHITE.getIndex());
		style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
		return style;
	}

	/**
	 * 
	 * @author Anthony Couture
	 * @param feuille du document
	 * @param style a appliquer aux cellules
	 * @return liste de ligne initialise avec le style en paramètre
	 * 
	 * <p>Cette fonction initialise une feuille en appliquant un style sur 94 lignes de 19 cellules</p>
	 */
	public static void init(XSSFSheet feuille) {
		for (int i = 0; i < nbLigne; i++) {
			Row ligne = feuille.createRow(i);
			for (int j = 0; j < nbColonne; j++) {
				Cell cellule = ligne.createCell(j);
				cellule.setCellStyle(styleGenerale);
			}
			liste.add(ligne);
			
		}
	}
	
	/**
	 * 
	 * @author Anthony Couture
	 * @param cell cellule ou faut mettre le nombre
	 * @param val l'entier a mettre dans la cellule
	 * 
	 * <p>Cette procedure permet d'inserer un entier dans une cellule avec ces coordonees</p>
	 */
	public static void insertCell (Cell cell, int val) {
		cell.setCellType(CellType.NUMERIC);
		cell.setCellValue(val);
	}
	
	/**
	 * 
	 * @author Anthony Couture
	 * @param cell cellule ou faut mettre la chaine de caractere
	 * @param val la chaine de caractere a mettre dans la cellule
	 * 
	 * <p>Cette procedure permet d'inserer une chaine de caractère dans une celulle avec ces coordonees</p>
	 */
	public static void insertCell (Cell cell, String val) {
		cell.setCellType(CellType.STRING);
		cell.setCellValue(val);
	}
	
	/**
	 * 
	 * @author Anthony Couture
	 * @param cell cellule ou faut mettre la formule
	 * @param val chaine de caractere correspondant a la formule a mettre dans la cellule
	 * 
	 * <p>Cette procedure permet d'inserer une formule dans une cellule avec ces coordonees</p>
	 */
	public static void insertCellFormule (Cell cell, String val) {
		cell.setCellType(CellType.FORMULA);
		cell.setCellFormula(val);
	}
	
	/**
	 * @author Anthony Couture
	 * @param cell Cellule ou on lui met une couleur
	 */
	public static void cellColor(Cell cell, short color) {
		XSSFCellStyle style = workbook.createCellStyle();
		style.cloneStyleFrom(cell.getCellStyle());
		style.setFillForegroundColor(color);
		cell.setCellStyle(style);
	}
	
	/**
	 * @author Anthony Couture
	 * @param cell Cellule ou on rajoute une bordure en dessous
	 */
	public static void bordureBas(Cell cell) {
		XSSFCellStyle style = workbook.createCellStyle();
		style.cloneStyleFrom(cell.getCellStyle());
		style.setBorderBottom(BorderStyle.MEDIUM);
		cell.setCellStyle(style);
	}
	
	/**
	 * @author Anthony Couture
	 * @param cell Cellule ou on rajoute une bordure au dessus
	 */
	public static void bordureHaut(Cell cell) {
		XSSFCellStyle style = workbook.createCellStyle();
		style.cloneStyleFrom(cell.getCellStyle());
		style.setBorderTop(BorderStyle.MEDIUM);
		cell.setCellStyle(style);
	}
	
	/**
	 * @author Anthony Couture
	 * @param cell Cellule ou on rajoute une bordure sur le cote gauche
	 */
	public static void bordureGauche(Cell cell) {
		XSSFCellStyle style = workbook.createCellStyle();
		style.cloneStyleFrom(cell.getCellStyle());
		style.setBorderLeft(BorderStyle.MEDIUM);
		cell.setCellStyle(style);
	}
	
	/**
	 * @author Anthony Couture
	 * @param cell Cellule ou on rajoute une bordure sur le cote droit
	 */
	public static void bordureDroit(Cell cell) {
		XSSFCellStyle style = workbook.createCellStyle();
		style.cloneStyleFrom(cell.getCellStyle());
		style.setBorderRight(BorderStyle.MEDIUM);
		cell.setCellStyle(style);
	}
	
	/**
	 * @author Anthony Couture
	 * @param liste liste contenant les differentes lignes
	 * @param feuille feuille ou nous devons creer la ligne
	 * @return la liste du paramètre avec une ligne en plus
	 */
	public static List<Row> addRow(List<Row> liste, XSSFSheet feuille) {
		Row ligne = feuille.createRow(nbLigne);
		nbLigne++;
		for(int i = 0; i<nbColonne ; i++) {
			Cell cell = ligne.createCell(i);
			cell.setCellStyle(styleGenerale);
		}
		liste.add(ligne);
		return liste;
	}
	
	/**
	 * @author Anthony couture
	 * @param liste liste contenant les differentes lignes
	 * @return la liste du paramètre avec une colonne en plus
	 */
	public static List<Row> addColonne(List<Row> liste) {
		for (Row row : liste) {
			Cell cell = row.createCell(nbColonne);
			cell.setCellStyle(styleGenerale);
		}
		nbColonne++;
		return liste;
	}
	
	/**
	 * 
	 * @author Anthony Couture
	 * @param cell cellule ou on insert une somme en euro
	 * @param val valeur en entier de la somme
	 */
	public static void insertCellEuro(Cell cell, int val) {
		XSSFCellStyle style = workbook.createCellStyle();
		style.cloneStyleFrom(cell.getCellStyle());
		cell.setCellStyle(style);
		style.setDataFormat(dataformat.getFormat(BuiltinFormats.getBuiltinFormat(5)));
		cell.setCellValue(val);
	}
	
	/**
	 * 
	 * @author Anthony Couture
	 * @param cell cellule a inserer la formule en euro
	 * @param val chaine de caractere de la formule
	 */
	public static void insertCellEuroFormula(Cell cell, String val) {
		XSSFCellStyle style = workbook.createCellStyle();
		style.cloneStyleFrom(cell.getCellStyle());
		cell.setCellStyle(style);
		style.setDataFormat(dataformat.getFormat(BuiltinFormats.getBuiltinFormat(5)));
		cell.setCellType(CellType.FORMULA);
		cell.setCellFormula(val);
	}
	
	/**
	 * 
	 * @throws IOException
	 * 
	 * <p>
	 * 		Cette procedure permet d'executer cette classe. <br>
	 * 		Elle realise les actions suivantes : 
	 * 		<ul>
	 * 			<li>Creation du Excel</li>
	 * 			<li>Creation du style</li>
	 * 			<li>Creation d'une feuille</li>
	 * 			<li>Creation du dataformat pour pouvoir mettre en euro</li>
	 * 			<li>Initialisation des variables pour l'initialisation</li>
	 * 			<li>Initialisation de la feuille</li>
	 * 			<li>On insere la valeur 10 dans la cellule A1</li>
	 * 			<li>On insere la valeur 13 dans la cellule A2</li>
	 * 			<li>On insere la formule A1+A2 dans la cellule A3</li>
	 * 			<li>On insere la valeur 12 dans la cellule C2</li>
	 * 			<li>On met en jaune la cellule C2</li>
	 * 			<li>On ajoute toutes les bordures a la cellule C2</li>
	 * 			<li>On insere une valeur de 200 euros dans la cellule F2</li>
	 * 			<li>On fait la formule F2*2 et on a le resultat en euro dans la celulle F3</li>
	 * 			<li>On ajoute une ligne au fichier avec le style</li>
	 * 			<li>On ajoute une colonne au fichier avec le style</li>
	 *			<li>Creation du fichier, ecriture des donnees a l'interieur</li>
	 *			<li>Message de fin avec le chemin du fichier</li>
	 * 		</ul>
	 * </p>
	 */
	public static void main(String[] args) throws IOException {
		workbook = new XSSFWorkbook();
		styleGenerale = createStyleGeneral(workbook);
		XSSFSheet feuille = workbook.createSheet("template");
		dataformat = workbook.createDataFormat();
		
		nbLigne = 94;
		nbColonne = 19;
		liste= new ArrayList<Row>();
		init(feuille);
		
		insertCell(liste.get(0).getCell(0), 10);
		insertCell(liste.get(1).getCell(0), 13);
		insertCellFormule(liste.get(2).getCell(0), "A1+A2");
		
		insertCell(liste.get(1).getCell(2),12);
		
		cellColor(liste.get(1).getCell(2), IndexedColors.YELLOW.getIndex());
		bordureBas(liste.get(1).getCell(2));
		bordureDroit(liste.get(1).getCell(2));
		bordureGauche(liste.get(1).getCell(2));
		bordureHaut(liste.get(1).getCell(2));
		
		insertCellEuro(liste.get(1).getCell(5), 200);
		insertCellEuroFormula(liste.get(2).getCell(5), "F2*2");
		
		liste = addRow(liste, feuille);
		
		liste = addColonne(liste);

		File file = new File("./docExcel.xlsx");
		file.getParentFile().mkdirs();

		FileOutputStream outFile = new FileOutputStream(file);
		workbook.write(outFile);
		System.out.println("Created file: " + file.getAbsolutePath());

	}

}
