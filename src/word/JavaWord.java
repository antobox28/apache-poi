package word;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.jws.soap.SOAPBinding.ParameterStyle;

import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

public class JavaWord {

	/**
	 * 
	 * @author Anthony Couture
	 * @param args
	 * @throws IOException
	 * 
	 * <p>
	 * 		Cette procedure permet d'executer cette classe. <br>
	 * 		Elle realise les actions suivantes : 
	 * 		<ul>
	 * 			<li>Creation du Word</li>
	 * 			<li>Creation d'un titre centré de taille 20</li>
	 * 			<li>Creation d'un paragraphe</li>
	 *			<li>Creation du fichier, ecriture des donnees a l'interieur</li>
	 *			<li>Message de fin avec le chemin du fichier</li>
	 * 		</ul>
	 * </p>
	 */
	public static void main(String[] args) throws IOException {
		XWPFDocument document= new XWPFDocument();
		
		XWPFParagraph titre =document.createParagraph();
		XWPFRun runTitre=titre.createRun();
		runTitre.setText("Titre");
		runTitre.setFontSize(20);
		titre.setAlignment(ParagraphAlignment.CENTER);
		
		XWPFParagraph paragraph = document.createParagraph();
		XWPFRun runParagraphe=paragraph.createRun();
		runParagraphe.setText("Paragraphe");
		
		
		
		File file = new File("./docWord.docx");
		file.getParentFile().mkdirs();
		FileOutputStream outFile = new FileOutputStream(file);
		document.write(outFile);
		System.out.println("Created file: " + file.getAbsolutePath());
		outFile.close();
	}
}
